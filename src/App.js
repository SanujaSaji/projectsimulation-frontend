import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import Wallets from "./components/Wallets/Wallets";

function App() {
    return (
        <div>
            <Wallets/>
        </div>
    );
}

export default App;
