import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Wallet from "./components/Wallet/Wallet";
import NavBar from "./components/NavBar";
import Transfer from "./components/Transfer/Transfer";

const routing = (
    <Router>
        <div>
            <NavBar />
            <Switch>
                <Route exact path="/" component={App}/>
                <Route path="/wallets/:id" component={Wallet}/>
                <Route path="/transfer" component={Transfer}/>
                {/*<Route path="/contact" component={Contact} />*/}
            </Switch>
        </div>
    </Router>
);

ReactDOM.render(routing, document.getElementById('root'));