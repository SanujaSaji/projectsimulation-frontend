import SelectInput from "./SelectInput";
import {shallow} from "enzyme";
import React from "react";

describe('Select Input', function () {
    it('should display select input', function () {
        const selectBox = <SelectInput id="selectInput" defaultOption={"Default"}
                                       data={[]}
                                       onOptionChange={() => {}}/>;
        const select = shallow(selectBox);

        expect(select.find('#selectInput')).toHaveLength(1);
    });

    it('should display placeholder based on the text passed through props', function () {
        const selectBox = <SelectInput id="selectInput" defaultOption={"Default"}
                                       data={[]}
                                       onOptionChange={() => {}}
                                       placeholder={"Placeholder"}/>;
        const select = shallow(selectBox);

        expect(select.find('#selectInput').prop('placeholder')).toEqual("Placeholder");
    });

    // TODO: Change test name to less verbose (refer manager)
    it('should display options based on the data passed through props', function () {
        const option1 = {id: '1', name: 'option1'};
        const selectBox = <SelectInput id="selectInput" defaultOption={"Default"}
                                       data={[option1]}
                                       onOptionChange={() => {}}
                                       placeholder={"Placeholder"}/>;
        const select = shallow(selectBox);

        let options = select.find('#selectInput').find('option');
        expect(options.at(1).prop('value')).toEqual("1");
        expect(options.at(1).text()).toEqual("option1");
    });

    it('should send data to the parent with the selected option value', function () {
        const option1 = {id: '1', name: 'option1'};
        const option2 = {id: '2', name: 'option2'};
        const mockFn = jest.fn();
        const selectBox = <SelectInput id="selectInput" defaultOption={"Default"}
                                       data={[option1, option2]}
                                       onOptionChange={mockFn("1")}
                                       placeholder={"Placeholder"}/>;
        const select = shallow(selectBox);
        let options = select.find('#selectInput').find('option');

        options.first().simulate('click', null);

        expect(mockFn).toHaveBeenCalledWith(options.at(1).prop('value'));
    });
});