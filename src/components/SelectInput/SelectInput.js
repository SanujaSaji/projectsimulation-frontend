import React, {Component} from 'react';

class SelectInput extends Component {
    constructor(props) {
        super(props);

        this.state = {};

    }

    render() {
        return (
            <div>
                <select className={"ui selection dropdown"} id={this.props.id} placeholder={this.props.placeholder} onChange={this.handleSelectInputChange}>
                    <option className="item" key={0} value={this.props.defaultOption}>{this.props.defaultOption}</option>
                    {this.props.data.map(option => {
                        return <option className="item" key={option.id} value={option.id}>
                            {option.name}
                        </option>;
                    })}
                </select>
            </div>
        );
    }

    handleSelectInputChange = (event) => {
        this.props.onOptionChange(event.target.id, parseInt(event.target.value));
    }
}

SelectInput.propTypes = {};

export default SelectInput;