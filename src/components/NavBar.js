import React, {Component} from 'react';

class NavBar extends Component {
    constructor(props) {
        super(props);

        this.state = {};

    }

    render() {
        return (
            <div className="ui massive inverted grey stackable menu">

                <a href="/" className="item active"><h3 className="ui header white inverted">Paysa</h3></a>
                <div className="right menu">
                    <a href={"/transfer"} className="ui item"><h3 className="ui header white inverted">Add
                        Transaction</h3></a>
                </div>
            </div>
        );
    }
}

NavBar.propTypes = {};

export default NavBar;