import React from 'react';
import WalletGrid from "./WalletGrid/WalletGrid";

Home.propTypes = {};

function Home(props) {
    return (
        <div className="ui segment">
            <WalletGrid/>
        </div>
    );
}

export default Home;