import TransactionTable from "./TransactionTable";
import {shallow} from "enzyme";
import React from "react";

describe('Transaction Table', function () {
    it('should display table five headers', function () {
        const table = shallow(<TransactionTable data={[]}/>);
        let headers = table.find('thead tr th');

        expect(headers).toHaveLength(5);
        expect(headers.at(0).text()).toEqual("Date");
        expect(headers.at(1).text()).toEqual("Remarks");
        expect(headers.at(2).text()).toEqual("Type");
        expect(headers.at(3).text()).toEqual("Amount");
        expect(headers.at(4).text()).toEqual("From/To Wallet");
    });

    it('should display transactions based on passed props data', function () {
        const transaction = {
            "id": 8,
            "creationDate": "2019-09-09T06:03:59.993+0000",
            "remarks": "Self",
            "type": "DEBIT",
            "amount": 100.0
        };
        const table = shallow(<TransactionTable data={[transaction]}/>);
        const rows = table.find('tbody tr');
        expect(rows).toHaveLength(1);

        const data = rows.find('td');

        expect(data).toHaveLength(5);
        expect(data.at(0).text()).toEqual("Sep 9, 2019, 11:33 AM")
    });
});