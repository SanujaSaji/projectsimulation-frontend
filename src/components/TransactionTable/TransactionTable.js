import React from 'react'
import './TransactionTable.css'
import PropTypes from 'prop-types';

function TransactionTable(props) {
    return (
        <div style={{"height": "500px", "overflowY": "scroll"}}>
            <table className="ui sortable teal celled padded very large table dividing divided"
                   border="1px solid black">
                <thead>
                <tr>
                    <th style={{"width": "20%"}}>Date</th>
                    <th style={{"width": "30%"}}>Remarks</th>
                    <th style={{"width": "10%"}}>Type</th>
                    <th style={{"width": "20%"}}><i className="rupee sign icon"/>Amount</th>
                    <th style={{"width": "20%"}}>From/To Wallet</th>
                </tr>
                </thead>
                <tbody>
                {
                    props.data.map((transaction) => {
                            const {creationDate, type, id, amount, remarks, fromToWallet} = transaction;
                            return (
                                <tr key={id}
                                    className={type === "CREDIT" ? "positive" : "negative"}>
                                    <td>{new Date(creationDate).toLocaleDateString('en-GB', {
                                        day: 'numeric',
                                        month: 'short',
                                        year: 'numeric',
                                        hour: 'numeric',
                                        minute: 'numeric'
                                    })}</td>
                                    <td>{remarks}</td>
                                    <td>{type}</td>
                                    <td>{amount}</td>
                                    <td>{fromToWallet}</td>
                                </tr>)
                        }
                    )
                }
                </tbody>
            </table>
        </div>
    );

}

TransactionTable.propTypes = {
    data: PropTypes.array.isRequired
};

export default TransactionTable;