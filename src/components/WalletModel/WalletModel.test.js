import React from "react";
import WalletModel from "./WalletModel";
import axios from "axios";

jest.mock('axios');

describe('Wallet Model', () => {
    it('should send axios get request to get wallet details', () => {
        axios.get.mockResolvedValue({});

        WalletModel.get(1);

        expect(axios.get).toHaveBeenCalled();
    });

    it('should send axios put request to add money', () => {
        axios.put.mockResolvedValue({});

        WalletModel.addMoney();

        expect(axios.put).toHaveBeenCalled();
    });

    it('should send axios post request to add wallet', () => {
        axios.post.mockResolvedValue({});

        WalletModel.addWallet();

        expect(axios.post).toHaveBeenCalled();
    });

    it('should send axios get request for wallets by userId', () => {
        axios.get.mockResolvedValue({});

        WalletModel.getAllByUserId();

        expect(axios.get).toHaveBeenCalled();
    });
});