import axios from "axios";

export default class WalletModel {
    static get(id) {
        return axios.get(process.env.REACT_APP_WALLET_API_URL + 'users/1/wallets/' + id)
            .then(response  => response.data)
            .catch();
    }

    static addMoney(id, amount) {
        return axios.put(process.env.REACT_APP_WALLET_API_URL + 'users/1/wallets/' + id, {balance: amount})
            .then(response => response.data)
            .catch();
    }

    static addWallet(name,balance) {
        return axios.post(process.env.REACT_APP_WALLET_API_URL + 'users/1/wallets', {name:name,balance:balance, user:{id:1}})
            .then(response => response.status)
            .catch((error) => error.response.status);
    }

    static getAllByUserId(id) {
        return axios.get(process.env.REACT_APP_WALLET_API_URL + 'users/' + id + '/wallets')
            .then(response => response.data)
            .catch();
    }

    static getBalanceColorCode(balance) {
        return parseFloat(balance) === 0 ? "color-red" :
            balance > 0 && balance <= 500 ? "color-orange" : "color-black";
    }

}