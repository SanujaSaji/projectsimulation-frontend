import WalletModel from "../WalletModel/WalletModel";
import React from "react";
import Wallets from "./Wallets";
import {shallow} from "enzyme";

let twoWallets = [
    {
        "id": 8,
        "name": "dvsdvcd",
        "balance": 232.0,
        "user": {
            "id": 1,
            "userName": "admin",
            "password": "password",
            "firstName": "John",
            "lastName": "Doe"
        },
    },
    {
        "id": 7,
        "name": "dvsdvc",
        "balance": 2324.0,
        "user": {
            "id": 1,
            "userName": "admin",
            "password": "password",
            "firstName": "John",
            "lastName": "Doe"
        }
    }];

jest.mock('../WalletModel/WalletModel');
describe('Wallets Component', () => {
    it('should send axios get request to get wallet details', async () => {
        WalletModel.getAllByUserId.mockResolvedValue(twoWallets);
        let wallets = shallow(<Wallets/>);

        await Promise.resolve();

        expect(wallets.state().wallets).toEqual(twoWallets);
    });
});