import React from "react";
import WalletGrid from "../WalletGrid/WalletGrid";
import AddWallet from "../AddWallet/AddWallet";
import WalletModel from "../WalletModel/WalletModel";

class Wallets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: 1,
            wallets: [],
            loading: false
        };
    }

    showLoader = (shouldShow) => {
        this.setState({
            loading: shouldShow
        });
    };

    componentDidMount() {
        this.getWalletsById();
    }

    getWalletsById = () => {
        WalletModel.getAllByUserId(this.state.userId)
            .then((wallets) => {
                this.setState({
                    wallets: wallets
                });
            });
    };

    render() {
        return (
            <div className={this.state.loading ? "ui segment" : ""}>
                <div className={this.state.loading ? "ui active inverted dimmer" : ""}>
                    <div className={this.state.loading ? "ui text loader" : ""}/>
                </div>
            <div id="wallets-div" style={{"margin": "2%"}}>
                <div className="ui top attached menu">
                    <div className="massive item">
                        <div className="ui teal huge header">
                            My Wallets
                        </div>
                    </div>
                    <AddWallet refreshWalletGrid={this.getWalletsById} showLoader={this.showLoader}/>
                </div>
                <WalletGrid wallets={this.state.wallets}/>
            </div></div>
        );
    }
}

Wallets.propTypes = {};

export default Wallets;