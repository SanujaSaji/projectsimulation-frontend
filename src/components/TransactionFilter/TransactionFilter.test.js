import React from "react";
import {shallow} from "enzyme";
import TransactionFilter from "./TransactionFilter";

describe('Transaction Filter', () => {
    it('should display an input box for remarks', () => {
        const transactionsFilter = shallow(<TransactionFilter onSetRemarksQuery={jest.fn} onSetTypeQuery={jest.fn}/>);

        expect(transactionsFilter.find("input#remarksFilter")).toHaveLength(1);
    });

    it('should display "Remarks" as place holder in input box', () => {
        const transactionsFilter = shallow(<TransactionFilter onSetRemarksQuery={jest.fn} onSetTypeQuery={jest.fn}/>);

        expect(transactionsFilter.find("input#remarksFilter").prop('placeholder')).toEqual("Remarks");
    });

    it('should call handleChange when value in search box is changed', () => {
        let event = {target: {value: 's'}};
        const mockFn = jest.fn();
        const transactionsFilter = shallow(<TransactionFilter onSetRemarksQuery={mockFn} onSetTypeQuery={jest.fn}/>);

        transactionsFilter.find('#remarksFilter').simulate('change', event);

        expect(mockFn).toHaveBeenCalled();
        expect(mockFn).toHaveBeenCalledWith(event.target.value);
    });

    it('should show select list with all transaction types', function () {
        const transactionsFilter = shallow(<TransactionFilter onSetRemarksQuery={jest.fn} onSetTypeQuery={jest.fn}/>);

        expect(transactionsFilter.find("select#typeFilter")).toHaveLength(1);
        expect(transactionsFilter.find("option#Both").text()).toEqual("Debit/Credit");
        expect(transactionsFilter.find("option#Credit").text()).toEqual("Credit");
        expect(transactionsFilter.find("option#Debit").text()).toEqual("Debit");
    });

    it('should call handleChangeOnType when value in type list is changed', () => {
        let event = {target: {value: 'Debit'}};
        const mockFn = jest.fn();
        const transactionsFilter = shallow(<TransactionFilter onSetTypeQuery={mockFn} onSetRemarksQuery={jest.fn}/>);

        transactionsFilter.find('#typeFilter').simulate('change', event);

        expect(mockFn).toHaveBeenCalled();
        expect(mockFn).toHaveBeenCalledWith(event.target.value);
    });
});