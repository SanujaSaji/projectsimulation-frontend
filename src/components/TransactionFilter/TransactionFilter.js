import React from 'react';
import PropTypes from 'prop-types';
import '../Transfer/Transfer.css'
TransactionFilter.propTypes = {
    onSetRemarksQuery: PropTypes.func.isRequired,
    onSetTypeQuery: PropTypes.func.isRequired
};

function TransactionFilter(props) {
    function handleChange(event) {
        props.onSetRemarksQuery(event.target.value);
    }

    function handleChangeOnType(event) {
        props.onSetTypeQuery(event.target.value);
    }

    return (
        <div className="ui segment" style={{float: "right"}}>
            <div className="ui top attached label">
                Filters
            </div>

            <div className="ui action left icon input">
                <div className="ui left icon input">
                    <input id="remarksFilter" placeholder={"Remarks"} onChange={handleChange}/>
                    <i className="filter icon"/>
                </div>
                <br className="break"/>
                <select id="typeFilter"
                        className="ui search dropdown"
                        onChange={handleChangeOnType}>
                    <option id="Both" value="" >Debit/ Credit</option>
                    <option id="Debit" value="Debit">Debit</option>
                    <option id="Credit" value="Credit">Credit</option>
                </select>
            </div>
        </div>
    );
}

export default TransactionFilter;