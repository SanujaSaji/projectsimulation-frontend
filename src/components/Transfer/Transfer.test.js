import Transfer from "./Transfer";
import React from "react";
import {shallow} from "enzyme";

describe('Transfer', function () {
    it('should display remarks label', function () {
        const transfer = shallow(<Transfer/>);

        expect(transfer.find('#remarksLabel').text()).toEqual("Remarks");
    });

    it('should display input for remarks', function () {
        const transfer = shallow(<Transfer/>);
        const remarksDiv = transfer.find('#remarks');

        expect(remarksDiv.find('#remarksInput')).toHaveLength(1);
    });

    it('should display placeholder as Remarks for remarks input', function () {
        const transfer = shallow(<Transfer/>);
        const remarksDiv = transfer.find('#remarks');

        expect(remarksDiv.find('#remarksInput').prop('placeholder')).toEqual("Remarks");
    });

    it('should display a select input for from wallet', function () {
        const transfer = shallow(<Transfer/>);
        const selectFrom = transfer.find('#from');

        expect(selectFrom).toHaveLength(1);
    });

    it('should display a select input for to wallet', function () {
        const transfer = shallow(<Transfer/>);
        const selectFrom = transfer.find('#to');

        expect(selectFrom).toHaveLength(1);
    });
});