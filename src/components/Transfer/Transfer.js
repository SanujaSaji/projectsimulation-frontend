import React, {Component} from 'react';
import SelectInput from "../SelectInput/SelectInput";
import WalletModel from "../WalletModel/WalletModel";
import {transferMoney} from "../TransactionService/TransactionService";

class Transfer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            from: 0,
            to: 0,
            wallets: [],
            amount: "",
            remarks: "",
            errorHidden: true,
            errorMessage: "",
            invalidAmountEntered: true,
            loading: false,
            transactionError: false,
            transactionStatus: false,
            transactionStatusMessage: ""
        };
    }

    componentDidMount() {
        this.getWalletsById();
    }

    getWalletsById = () => {
        WalletModel.getAllByUserId(1)
            .then((wallets) => {
                this.setState({
                    wallets: wallets
                });
            });
    };

    handleOptionChange = (selectElementId, walletId) => {
        this.setState({
            transactionStatus: false,
            [selectElementId]: walletId
        });
    };

    filteredOptions(select) {
        const wallets = this.state.wallets;
        if (select === "from") {
            return wallets.filter(wallet => wallet.id !== this.state.to);
        }
        if (select === "to") {
            return wallets.filter(wallet => wallet.id !== this.state.from);
        }
    }

    handleAmountChange = (event) => {
        this.setState({
            transactionStatus: false
        });
        const value = event.target.value;
        if (this.isValidInput(value)) {
            this.setState({
                errorHidden: true,
                amount: event.target.value,
                invalidAmountEntered: false
            });
            return;
        }
        this.setState({
            amount: event.target.value,
            errorHidden: false,
            errorMessage: "Invalid input",
            invalidAmountEntered: true
        });
    };

    isValidInput = (amount) => {
        let numberWithTwoDecimals = /^\d*\.\d{1,2}$/;
        let integer = /^\d+$/;
        let empty = /^$/;
        amount = amount.toString();
        return (amount.match(numberWithTwoDecimals) || amount.match(integer) || amount.match(empty)) && !(parseFloat(amount) === 0);
    };

    showLoader = (showLoader) => {
        this.setState({
            loading: showLoader
        });
    };

    handleClick = () => {
        this.setState({
            errorHidden: true,
        });
        this.showLoader(true);
        const transaction = {
            remarks: this.state.remarks,
            type: 'DEBIT',
            amount: this.state.amount
        };

        transferMoney(this.state.from, this.state.to, transaction).then(response => {
            this.showLoader(false);
            this.setState({
                transactionStatus: true,
                transactionError: false,
                transactionStatusMessage: "Transaction Successful! Make a new transaction",
                from: 0,
                to: 0,
                amount: "",
                remarks: ""
            });
            document.getElementById('from')[0].selected = true;
            document.getElementById('to')[0].selected = true;

        }).catch(error => {
            this.showLoader(false);
            this.setState({
                transactionStatus: true,
                transactionError: true,
                transactionStatusMessage: error.response.data.message
            });
        });
    };

    render() {
        return (
            <React.Fragment>
                <div className={"ui segment"}>
                    <div className={this.state.loading ? "ui active inverted dimmer" : ""}>
                        <div className={this.state.loading ? "ui text loader" : ""}/>
                    </div>
                    <div className="ui big segments" style={{marginLeft: '18px', textAlign: 'center'}}>
                        <div className="ui segment grey">
                            <div id="remarks">
                                <label id="remarksLabel" className="remarksLabel">
                                    Remarks{"   "}
                                </label>
                                <div className=" ui input focus ">

                                <input id="remarksInput"
                                       className="remarks"
                                       value={this.state.remarks}
                                       type="text"
                                       placeholder="Remarks"
                                       onChange={this.handleRemarksInputChange}>
                                </input>
                                </div>
                            </div>
                        </div>
                        <div className="ui big horizontal segments">
                            <div className="ui segment ">
                                <div className="ui labelled input">From :
                                    <SelectInput id="from"
                                                 defaultOption={"Select Wallet"}
                                                 data={this.filteredOptions("from")}
                                                 onOptionChange={this.handleOptionChange}>
                                    </SelectInput>
                                </div>
                            </div>
                            <div className="ui segment ">
                                <div className="ui labelled input">To :
                                    <SelectInput id="to"
                                                 defaultOption={"Select Wallet"}
                                                 data={this.filteredOptions("to")}
                                                 onOptionChange={this.handleOptionChange}>
                                    </SelectInput>
                                </div>
                            </div>
                        </div>
                        <div className="ui segment">
                            <div id="amount"
                                 className="ui labeled input">
                                <div id="addMoneyError"
                                     hidden={this.state.errorHidden}
                                     className={this.state.errorHidden ? "" : "ui right pointing red basic label"}>{this.state.errorMessage}
                                </div>
                                <div className="ui basic label">
                                    <i className="rupee sign icon"/>
                                </div>
                                <div className='ui input focus'>
                                    <input id="inputAddMoney"
                                           type="text"
                                           placeholder="Enter amount"
                                           onChange={this.handleAmountChange}
                                           value={this.state.amount}>
                                    </input>
                                </div>
                            </div>
                            <div className="ui segment">
                                <div style={{"padding": "10px"}}>
                                    <button
                                        className={this.state.invalidAmountEntered ? "ui teal button disabled" : "ui teal button"}
                                        id="buttonTransferMoney"
                                        onClick={this.handleClick}>Submit
                                    </button>
                                </div>
                                {
                                    this.state.transactionStatus ?
                                        <div id={"transactionStatus"}
                                             className={this.state.transactionError ? "ui red basic label" : "ui green basic label"}>
                                            {this.state.transactionStatusMessage}
                                        </div> : ""
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }

    handleRemarksInputChange = (event) => {
        this.setState({
            remarks: event.target.value,
            transactionStatus: false
        });
    }
}

Transfer.propTypes = {};

export default Transfer;