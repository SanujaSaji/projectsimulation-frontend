import React from "react";
import WalletModel from "../WalletModel/WalletModel";
import PropTypes from "prop-types";

export default function WalletCard(props) {
    return (<a key={props.wallet.id} className="ui raised link grey card" href={'/wallets/' + props.wallet.id}>
        <div className="content">
            <div className="header"
                 style={{"padding": "1%", "margin": "1%", "fontSize": "25px"}}> {props.wallet.name} </div>
            <div className="header" style={{"padding": "1%", "margin": "1%"}}>
                Balance:
                <span className={WalletModel.getBalanceColorCode(props.wallet.balance)}>
                        &nbsp;₹ {props.wallet.balance.toFixed(2)}
                    </span>
            </div>
        </div>
    </a>);
}

WalletCard.propTypes = {
    wallet: PropTypes.object.isRequired
};