import {shallow} from "enzyme";
import React from "react";
import AddMoney from "./AddMoney";
import WalletModel from "../WalletModel/WalletModel";
import TransactionModel from "../TransactionModel/TransactionModel";

jest.mock('../WalletModel/WalletModel');
jest.useFakeTimers();


describe('Add Money Component', () => {
    it('should display text box with id inputAddMoney', () => {
        const addMoney = shallow(<AddMoney/>);

        expect(addMoney.find('input')).toHaveLength(1);
        expect(addMoney.find('#inputAddMoney')).toHaveLength(1);
    });

    it('should display add button with id buttonAddMoney and text Add', () => {
        const addMoney = shallow(<AddMoney/>);

        expect(addMoney.find('button')).toHaveLength(1);
        expect(addMoney.find('#buttonAddMoney')).toHaveLength(1);
        expect(addMoney.find('#buttonAddMoney').text()).toEqual("Add");
    });

    it('should display error message when input is 0', () => {
        const addMoney = shallow(<AddMoney/>);

        addMoney.find('#inputAddMoney').simulate('change', {target: {value: 0}});
        expect(addMoney.find('#addMoneyError')).toHaveLength(1);
    });

    it('should display error message when input is -1', () => {
        const addMoney = shallow(<AddMoney/>);

        addMoney.find('#inputAddMoney').simulate('change', {target: {value: -1}});
        expect(addMoney.find('#addMoneyError')).toHaveLength(1);
    });

    it('should display error message when input is text', () => {
        const addMoney = shallow(<AddMoney/>);

        addMoney.find('#inputAddMoney').simulate('change', {target: {value: "sometext"}});
        expect(addMoney.find('#addMoneyError')).toHaveLength(1);
    });

    it('should display error message when transaction fails', () => {
        const addMoney = shallow(<AddMoney/>);

        addMoney.find('#inputAddMoney').simulate('change', {target: {value: 0}});
        expect(addMoney.find('#addMoneyError').text()).toEqual("Invalid input");
    });

    it('should send PUT request to Wallet when add button is clicked ', async () => {
        WalletModel.addMoney.mockResolvedValue({id: 9, name: "wallet-1", balance: 250});
        const addMoney = shallow(<AddMoney id={9} showLoader={jest.fn()}/>);
        addMoney.find('#inputAddMoney').simulate('change', {target: {value: 200}});
        addMoney.find('#buttonAddMoney').simulate('click');
        await Promise.resolve();

        expect(WalletModel.addMoney).not.toBeCalled();

        jest.runAllTimers();

        expect(WalletModel.addMoney).toHaveBeenCalled();
    });

    it('should send POST request to transaction when add button is clicked ', async () => {
        const transactionSpy = jest.spyOn(TransactionModel, 'post');

        TransactionModel.post();
        await Promise.resolve();

        expect(transactionSpy).toHaveBeenCalled();
    });
});