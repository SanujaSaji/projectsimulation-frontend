import React, {Component} from 'react';
import WalletModel from "../WalletModel/WalletModel";
import TransactionModel from "../TransactionModel/TransactionModel";

class AddMoney extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: "",
            errorHidden: true,
            errorMessage: "",
            invalidAmountEntered: true
        }
    }

    handleChange = (event) => {
        const value = event.target.value;
        if (this.isValidInput(value)) {
            this.setState({
                errorHidden: true,
                amount: event.target.value,
                invalidAmountEntered: false
            });
            return;
        }
        this.setState({
            amount: event.target.value,
            errorHidden: false,
            errorMessage: "Invalid input",
            invalidAmountEntered: true
        });
    };

    isValidInput = (amount) => {
        let numberWithTwoDecimals = /^\d*\.\d{1,2}$/;
        let integer = /^\d+$/;
        let empty = /^$/;
        amount = amount.toString();
        return (amount.match(numberWithTwoDecimals) || amount.match(integer) || amount.match(empty)) && !(parseFloat(amount) === 0);
    };

    handleClick = () => {
        this.setState({
            errorHidden: true
        });
        this.props.showLoader(true);
        setTimeout(() => {
            WalletModel.addMoney(this.props.id, this.state.amount)
                .then(() => {
                    this.props.onAdd();
                    TransactionModel.post(this.props.id, this.state.amount)
                        .then(() => {
                            this.setState({
                                amount: "",
                                invalidAmountEntered: true
                            });
                        });
                    this.props.showLoader(false);
                })
                .catch(() => {
                    this.props.showLoader(false);
                    this.setState({
                        errorHidden: false,
                        errorMessage: "Transaction failed"
                    });
                });
        }, 500);
    };

    render() {
        return <div className="ui segment">
            <div className="ui header">Add Money</div>
            <div className="ui right labeled input">
                <div id="addMoneyError" hidden={this.state.errorHidden}
                     className={this.state.errorHidden ? "" : "ui right pointing red basic label"}>{this.state.errorMessage}</div>
                <div className="ui basic label"><i className="rupee sign icon"/></div>
                <input id="inputAddMoney" type="text" placeholder="Enter amount" onChange={this.handleChange}
                       value={this.state.amount}/>
                <button className="ui teal tag label" id="buttonAddMoney"
                        onClick={this.handleClick} disabled={this.state.invalidAmountEntered}>Add
                </button>
            </div>
        </div>
    }
}

export default AddMoney;