export const mockPlaySoundFile = jest.fn().mockResolvedValue([{remarks: 'pppp'}]);
const mock = jest.fn().mockImplementation(() => {
    return {getByWalletId: mockPlaySoundFile};
});

export default mock;