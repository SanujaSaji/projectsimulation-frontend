import axios from "axios";

export function getByWalletId(walletId) {
    return axios.get(process.env.REACT_APP_WALLET_API_URL + 'users/1/wallets/' + walletId + '/transactions')
        .then(response => response.data)
        .catch();
}

export function transferMoney(fromWallet, toWallet, transaction){
    return axios.post(process.env.REACT_APP_WALLET_API_URL + 'users/1/wallets/' + fromWallet + '/transfer',
        {
            fromWallet:{
                id: fromWallet,
                user:{
                    id: 1
                }
            },
            toWallet:{
                id: toWallet,
                user:{
                    id: 1
                }
            },
            transaction: transaction
        })
        .then(response  => response.data)
}