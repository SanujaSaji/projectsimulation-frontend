import {getByWalletId, transferMoney} from "./TransactionService";

const axios = require('axios');
jest.mock('axios');

describe('Transaction Service', () => {
    it('should send axios get request to get transactions by wallet id', () => {
        axios.get.mockResolvedValue({});

        getByWalletId(1);

        expect(axios.get).toHaveBeenCalled();
    });

    it('should send axios post request to transfer money between wallets', () => {
        axios.post.mockResolvedValue({});
        const transaction = {remarks: '', type: 'CREDIT', amount: 100};
        transferMoney(1, 2, transaction);

        expect(axios.post).toHaveBeenCalled();
    });
});