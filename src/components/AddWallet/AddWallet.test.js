import {shallow} from "enzyme";
import React from "react";
import AddWallet from "./AddWallet";
import WalletModel from "../WalletModel/WalletModel";

jest.mock('../WalletModel/WalletModel');

describe("Add Wallet Component", () => {
    it('should disable add-wallet button as long as name field empty', function () {
        const addWallet = shallow(<AddWallet refreshWalletGrid={jest.fn} showLoader={jest.fn}/>);
        addWallet.find('#inputWalletName');
        let button = addWallet.find('#buttonAddWallet');

        expect(button).toHaveLength(1);
        expect(button.hasClass('disabled'));
    });

    it('should allow entering only alpha-numeric characters in wallet name input box', function () {
        const addWallet = shallow(<AddWallet refreshWalletGrid={jest.fn} showLoader={jest.fn}/>);
        let walletName = addWallet.find('#inputWalletName');
        walletName.simulate('change', {target: {value: "??"}});

        expect(addWallet.find('#walletNameError')).toHaveLength(1);
    });

    it('should allow entering numbers with at-most 2 decimals in wallet balance input box', function () {
        const addWallet = shallow(<AddWallet refreshWalletGrid={jest.fn} showLoader={jest.fn}/>);
        let walletBalance = addWallet.find('#inputWalletBalance');

        walletBalance.simulate('change', {target: {value: "??"}});
        expect(addWallet.find('#walletBalanceError')).toHaveLength(1);

        walletBalance.simulate('change', {target: {value: "1.111"}});
        expect(addWallet.find('#walletBalanceError')).toHaveLength(1);

        walletBalance.simulate('change', {target: {value: "1"}});
        expect(addWallet.find('#walletBalanceError').text()).toEqual("");
    });

    it('should send POST request to Wallets when add wallet button is clicked ', async () => {
        WalletModel.addWallet.mockResolvedValue({name: "wallet1", balance: 0});
        const addWallet = shallow(<AddWallet refreshWalletGrid={jest.fn} showLoader={jest.fn}/>);
        addWallet.find('#inputWalletName').simulate('change', {target: {value: "wallet1"}});
        addWallet.find('#buttonAddWallet').simulate('click');
        await Promise.resolve();

        expect(WalletModel.addWallet).toHaveBeenCalled();
    });

    xit('should show Wallet Already Exists error when POST request is made with existing wallet name ', async () => {
        WalletModel.addWallet.mockRejectedValue({response: {status: 409}});
        const addWallet = shallow(<AddWallet refreshWalletGrid={jest.fn} showLoader={jest.fn}/>);
        addWallet.find('#inputWalletName').simulate('change', {target: {value: "wallet1"}});
        addWallet.find('#buttonAddWallet').simulate('click');

        await Promise.resolve();
        await Promise.resolve();

        expect(WalletModel.addWallet).toHaveBeenCalled();
        expect(addWallet.find('#walletNameError').text()).toEqual("Wallet name already exists");
    });

    xit('should send callback to parent on successful add wallet', async () => {
        WalletModel.addWallet.mockResolvedValue({response: {status: 201}});
        const addWallet = shallow(<AddWallet refreshWalletGrid={jest.fn} showLoader={jest.fn}/>);
        addWallet.find('#inputWalletName').simulate('change', {target: {value: "wallet1"}});
        addWallet.find('#buttonAddWallet').simulate('click');

        await Promise.resolve();

        expect(WalletModel.addWallet).toHaveBeenCalled();
        expect(addWallet.props().refreshWalletGrid).toHaveBeenCalled();
    });
});