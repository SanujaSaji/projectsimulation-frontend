import React from "react";
import WalletModel from "../WalletModel/WalletModel";
import PropTypes from 'prop-types';

class AddWallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            walletNameErrorHidden: true,
            walletBalanceErrorHidden: true,
            walletNameErrorMessage: "",
            invalidWalletNameErrorMessage: "Invalid name. Should be atmost 20 characters including letters/numbers, ",
            walletNameAlreadyExistsMessage: "Wallet name already exists",
            invalidBalanceErrorMessage: "Invalid balance. Can only be a number with at-most 2 decimals",
            walletBalanceErrorMessage: "",
            walletName: "",
            walletBalance: 0
        };

    }

    handleNameChange = (event) => {
        let alphaNumericWithSpaces = /^[0-9a-zA-Z ]{0,20}$/;
        let input = event.target.value;
        let inputWords = input.split(" ").filter(a => a !== "");
        let trimmedInput = inputWords.length !== 0 ? inputWords.reduce((a, c) => a = a + " " + c) : "";
        if (input.match(alphaNumericWithSpaces) && trimmedInput === input) {
            this.setState({
                walletNameErrorHidden: true,
                walletName: input
            });
        } else {
            this.setState({
                walletNameErrorMessage: this.state.invalidWalletNameErrorMessage,
                walletNameErrorHidden: false,
                walletName: input
            });
        }
    };

    handleBalanceChange = (event) => {
        let numberWithTwoDecimals = /^\d*\.\d{1,2}$/;
        let integer = /^\d+$/;
        let input = event.target.value;
        if (input.match(numberWithTwoDecimals) || input.match(integer) || input.match(/^$/)) {
            this.setState({
                walletBalanceErrorHidden: true,
                walletBalanceErrorMessage: "",
                walletBalance: input
            });
        } else {
            this.setState({
                walletBalanceErrorMessage: this.state.invalidBalanceErrorMessage,
                walletBalanceErrorHidden: false,
                walletBalance: input
            });
        }
    };

    handleClick = () => {
        this.props.showLoader(true);
        WalletModel.addWallet(this.state.walletName, this.state.walletBalance)
            .then((responseCode) => {
                if (responseCode === 409) {
                    this.setState({
                        walletNameErrorHidden: false,
                        walletNameErrorMessage: this.state.walletNameAlreadyExistsMessage
                    });
                }
                if (responseCode === 201) {
                    this.props.refreshWalletGrid();
                }
                this.props.showLoader(false);
            });
    };

    render() {
        return (
            <div className="right menu">
                <div className="item">
                    <div className="item">
                        <div className="ui input">
                            <div id="walletNameError"
                                 className={this.state.walletNameErrorHidden ? "" : "ui right pointing red basic label"}
                                 hidden={this.state.walletNameErrorHidden}>
                                {this.state.walletNameErrorMessage}
                            </div>
                            <input id="inputWalletName"
                                   type="text"
                                   placeholder="Enter Wallet Name"
                                   onChange={this.handleNameChange}
                                   value={this.state.walletName}
                                   maxLength={20}
                            />
                        </div>
                    </div>

                    <div className="item">
                        <div className="ui input">
                            <div id="walletBalanceError"
                                 className={this.state.walletBalanceErrorHidden ? "" : "ui right pointing red basic label"}
                                 hidden={this.state.walletBalanceErrorHidden}>
                                {this.state.walletBalanceErrorMessage}
                            </div>
                            <div className="ui basic label">
                                <i className="rupee sign icon"/>
                            </div>
                            <input id="inputWalletBalance"
                                   type="number"
                                   placeholder="Enter Balance"
                                   onChange={this.handleBalanceChange}
                                   value={this.state.walletBalance}
                            />
                        </div>
                    </div>
                </div>
                <div className="item">
                    <button
                        className={this.state.walletName && (this.state.walletNameErrorHidden && this.state.walletBalanceErrorHidden) ? "ui teal button" : "ui teal button disabled"}
                        id="buttonAddWallet"
                        onClick={this.handleClick}>Add Wallet
                    </button>
                </div>
            </div>
        );
    }
}

AddWallet.propTypes = {
    refreshWalletGrid: PropTypes.func.isRequired
};

export default AddWallet;