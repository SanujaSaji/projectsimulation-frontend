import {shallow} from "enzyme";
import React from "react";
import WalletTransactions from "../WalletTransactions/WalletTransactions";
import TransactionFilter from "../TransactionFilter/TransactionFilter";
import TransactionTable from "../TransactionTable/TransactionTable";
import * as TransactionService from "../TransactionService/TransactionService";

jest.mock('../TransactionService/TransactionService');

function createPromise(returnValue) {
    return new Promise(function (resolve) {
        resolve(returnValue);
    });
}

describe('Wallet Transactions', function () {
    beforeAll(() => {
        TransactionService.getByWalletId = jest.fn();
        TransactionService.getByWalletId.mockResolvedValue([]);
    });
    it('should display TransactionsFilter component', () => {
        const walletTransactions = shallow(<WalletTransactions/>);

        expect(walletTransactions.find(TransactionFilter)).toHaveLength(1);
    });

    it('should display Transactions as heading', async function () {
        const walletTransactions = shallow(<WalletTransactions getByWalletId={() => createPromise([])}/>);
        await Promise.resolve();

        expect(walletTransactions.find('#transactionsHeading').text()).toEqual("Transactions");
    });

    it('should not display transactions table when there are no transactions', async function () {
        const walletTransactions = shallow(<WalletTransactions getByWalletId={() => createPromise([])}/>);
        await Promise.resolve();
        const transactionsTableDiv = walletTransactions.find('#transactionsTable');
        expect(transactionsTableDiv).toHaveLength(1);
        const transactionsTable = transactionsTableDiv.find('table');

        expect(transactionsTable).toHaveLength(0);
    });

    it('should display a message when no transactions', async function () {
        const walletTransactions = shallow(<WalletTransactions getByWalletId={() => createPromise([])}/>);

        await Promise.resolve();
        const transactionsTableDiv = walletTransactions.find('#transactionsTable');
        expect(transactionsTableDiv).toHaveLength(1);

        expect(transactionsTableDiv.find('#noTransactionsMessage')).toHaveLength(1);
        expect(transactionsTableDiv.find('#noTransactionsMessage').text()).toEqual("No transactions yet.");
    });

    xit('should display transactions table when there is atleast one transaction', async () => {
        const testTransaction = {};
        const walletTransactions = shallow(<WalletTransactions
            getByWalletId={() => createPromise([testTransaction])}/>);

        await Promise.resolve();
        const transactionsTableDiv = walletTransactions.find('#transactionsTable');
        expect(transactionsTableDiv).toHaveLength(1);

        expect(transactionsTableDiv.find('#noTransactionsMessage')).toHaveLength(0);
        expect(transactionsTableDiv.find(TransactionTable)).toHaveLength(1);
    });

    it('should filter by remarks', async () => {
        const transactions = [{remarks: 'pizza', type: ''}, {remarks: 'cab', type: ''}];
        TransactionService.getByWalletId.mockResolvedValue(transactions);
        const walletTransactions = shallow(<WalletTransactions/>);
        await Promise.resolve();
        const transactionFilter = walletTransactions.find(TransactionFilter);

        transactionFilter.simulate('setRemarksQuery', 'pizza');

        const transactionTable = walletTransactions.find(TransactionTable);
        expect(transactionTable.props().data).toHaveLength(1);
    });

    it('should filter by type', async () => {
        const transactions = [{remarks: 'pizza', type: 'Debit'}, {remarks: 'cab', type: 'Credit'}];
        TransactionService.getByWalletId.mockResolvedValue(transactions);
        const walletTransactions = shallow(<WalletTransactions/>);
        await Promise.resolve();
        const transactionFilter = walletTransactions.find(TransactionFilter);

        transactionFilter.simulate('setTypeQuery', 'Debit');

        const transactionTable = walletTransactions.find(TransactionTable);
        expect(transactionTable.props().data).toHaveLength(1);
    });
});