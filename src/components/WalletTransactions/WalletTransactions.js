import React, {Component} from 'react';
import TransactionTable from "../TransactionTable/TransactionTable";
import TransactionFilter from "../TransactionFilter/TransactionFilter";
import * as TransactionService from "../TransactionService/TransactionService";

class WalletTransactions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            transactions: [],
            remarksSearchQuery: '',
            typeSearchQuery: ''
        };
    }

    componentDidMount() {
        this.getById();
    }

    UNSAFE_componentWillReceiveProps() {
        this.getById();
    }

    updateRemarksQuery = (query) => {
        this.setState({
            remarksSearchQuery: query
        });
    };

    updateTypeQuery = (query) => {
        this.setState({
            typeSearchQuery: query
        });
    };

    getById = () => {
        const {walletId} = this.props;
        TransactionService.getByWalletId(walletId)
            .then((transactions) => {
                this.setState({
                    transactions: transactions
                });
            });
    };

    filterTransactions = () => {
        return this.state.transactions.filter((transaction) =>
            transaction.remarks.toLowerCase().includes(this.state.remarksSearchQuery.toLowerCase()) &&
            transaction.type.toLowerCase().includes(this.state.typeSearchQuery.toLowerCase())
        );
    };

    render() {
        return (
            <div className="ui large section">
                <div>
                    <span className="ui huge header" id="transactionsHeading">Transactions</span>
                    <TransactionFilter onSetRemarksQuery={this.updateRemarksQuery}
                                       onSetTypeQuery={this.updateTypeQuery}/>
                </div>
                <br/><br/><br/><br/><br/>
                <div id="transactionsTable">
                    {(() => {
                        if (this.state.transactions.length === 0) {
                            return <h3 id="noTransactionsMessage">No transactions yet.</h3>
                        } else {
                            return <TransactionTable data={this.filterTransactions()}/>
                        }
                    })()}
                </div>
            </div>
        );
    }
}

WalletTransactions.propTypes = {};

export default WalletTransactions;