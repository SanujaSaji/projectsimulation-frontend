import React, {Component} from 'react';
import AddMoney from "../AddMoney/AddMoney";
import WalletModel from "../WalletModel/WalletModel";
import './Wallet.css';
import WalletTransactions from "../WalletTransactions/WalletTransactions";
import {getByWalletId} from '../TransactionService/TransactionService';

class Wallet extends Component {
    constructor(props) {
        super(props);
        const {params} = props.match;
        this.state = {
            id: params.id,
            name: "",
            balance: "",
            loading: false
        }
    }

    componentDidMount() {
        this.getWallet();
    }

    getWallet = () => {
        WalletModel.get(this.state.id)
            .then((wallet) => {
                this.setState({
                    id: wallet.id,
                    name: wallet.name,
                    balance: parseFloat(wallet.balance).toFixed(2)
                })
            });
    };

    showLoadingOverlay = (showLoader) => {
        this.setState({
            loading: showLoader
        });
    };

    render() {
        return <div className="ui segment" style={{"margin": "2%"}}>
            <div className={this.state.loading ? "ui segment" : ""}>
                <div className={this.state.loading ? "ui active inverted dimmer" : ""}>
                    <div className={this.state.loading ? "ui text loader" : ""}/>
                </div>
                <div style={{float: "right"}}>
                    <AddMoney id={this.state.id} onAdd={this.getWallet} showLoader={this.showLoadingOverlay}/>
                </div>
                <div className="ui card">
                    <div className="content">
                        <h1 id="name" className="ui header">{this.state.name}</h1>
                        <h2 className="ui header">
                            <span>Balance: </span>
                            <span id="balance" className={WalletModel.getBalanceColorCode(this.state.balance)}>
                                <i className="rupee sign icon"/>{this.state.balance}</span>
                        </h2>
                    </div>
                </div>
                <div className="ui segment">
                    <WalletTransactions walletId={this.state.id} getByWalletId={getByWalletId}/>
                </div>
            </div>
        </div>
    }
}

export default Wallet;