import {shallow} from "enzyme";
import React from "react";
import Wallet from "./Wallet";
import WalletModel from "../WalletModel/WalletModel";
import WalletTransactions from "../WalletTransactions/WalletTransactions";

jest.mock('../WalletModel/WalletModel');

describe('Wallet Component', () => {
    it('should display Wallet name', () => {
        WalletModel.get.mockResolvedValue({});
        const wallet = shallow(<Wallet match={{params:{id:1}}}/>);

        expect(wallet.find('#name')).toHaveLength(1);
    });

    it('should display wallet balance', () => {
        WalletModel.get.mockResolvedValue({});
        const wallet = shallow(<Wallet match={{params:{id:1}}}/>);

        expect(wallet.find('#balance')).toHaveLength(1);
    });

    it('should do GET request', async () => {
        WalletModel.get.mockResolvedValue({name:"wallet-1",balance:100});
        const wallet = shallow(<Wallet match={{params:{id:1}}}/>);

        await Promise.resolve();

        expect(WalletModel.get).toHaveBeenCalledWith(1);
        expect(wallet.find('#balance').text()).toEqual("100.00");
        expect(wallet.find('#name').text()).toEqual("wallet-1");
    });

    it('should display wallet balance in color received from wallet model', async () => {
        WalletModel.get.mockResolvedValue({name: "wallet-1", balance: 250});
        const wallet = shallow(<Wallet match={{params:{id:1}}}/>);

        await Promise.resolve();

        expect(WalletModel.get).toHaveBeenCalledWith(1);
        expect(wallet.find('#balance').text()).toEqual("250.00");
        expect(wallet.find('#name').text()).toEqual("wallet-1");
        expect(WalletModel.getBalanceColorCode).toHaveBeenCalledWith("250.00");
    });

    it('should render Wallet Transactions component', function () {
        WalletModel.get.mockResolvedValue({});

        const wallet = shallow(<Wallet match={{params:{id:1}}}/>);
        expect(wallet.find(WalletTransactions)).toHaveLength(1);
    });
});