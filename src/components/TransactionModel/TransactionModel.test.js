import React from "react";
import TransactionModel from "./TransactionModel";

const axios = require('axios');

jest.mock('axios');

describe('Transaction Model', () => {
    it('should send axios post request', () => {
        axios.post.mockResolvedValue({});

        TransactionModel.post();

        expect(axios.post).toHaveBeenCalled();
    });

});