import axios from "axios";

export default class TransactionModel {
    static post(id, amount) {
        return axios.post(process.env.REACT_APP_WALLET_API_URL + 'users/1/wallets/' + id + '/transactions',
            {
                remarks: "Self",
                type: "CREDIT",
                amount: amount
            })
            .then(response  => response.data)
            .catch();
    }
}