import {shallow} from "enzyme";
import React from "react";
import WalletGrid from "./WalletGrid";
import WalletCard from "../WalletCard/WalletCard";

let twoWallets = [
    {
        "id": 8,
        "name": "dvsdvcd",
        "balance": 232.0,
        "user": {
            "id": 1,
            "userName": "admin",
            "password": "password",
            "firstName": "John",
            "lastName": "Doe"
        },
    },
    {
        "id": 7,
        "name": "dvsdvc",
        "balance": 2324.0,
        "user": {
            "id": 1,
            "userName": "admin",
            "password": "password",
            "firstName": "John",
            "lastName": "Doe"
        }
    }];

describe('Wallet Grid', () => {
    it('should display wallet cards', () => {
        const walletGrid = shallow(<WalletGrid wallets={twoWallets}/>);

        let walletCards = walletGrid.find(WalletCard);
        expect(walletCards).toHaveLength(2);
    });
});