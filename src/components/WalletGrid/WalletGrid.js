import React from 'react';
import PropTypes from 'prop-types';
import WalletCard from "../WalletCard/WalletCard";

export default function WalletGrid(props) {
    return (
        <div className="ui attached segment" style={{"height": "434px", "overflowY": "scroll"}}>
            <div className="ui large section">
                <div className="ui three cards">
                    {props.wallets.map((wallet, index) => {
                        return <WalletCard key={index} wallet={wallet}/>;
                    })}
                </div>
            </div>
        </div>

    );
}
WalletGrid.propTypes = {
    wallets: PropTypes.array.isRequired
};