FROM node:10.16.3

CMD mkdir build
COPY ./build ./build

RUN npm run build --prod
CMD serve -s build